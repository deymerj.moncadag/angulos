import serial
import numpy
import matplotlib.pyplot as plt
import numpy.matlib as npm
from copy import copy, deepcopy
import time
import math
import requests

myAPI='O42JL8ODLRLH8DPU'
baseURL='https://api.thingspeak.com/update?api_key=%s' % myAPI

SENSITIVI_ACCEL=2/32768
SENSITIVI_GYRO=250/32768
#offsets=[472.92, -150.92, 177.6800000000003, 192.1, -41, 32.72]
A=0.6
B=0.4
dt=0.01
rad2dec=180/3.141592

#data = serial.Serial('/dev/ttyACM0',115200,timeout=10)
data = serial.Serial('COM3',115200,timeout=10)
print(data)

raw=100

datos=numpy.zeros((raw,8))
datos1=numpy.zeros((raw,8))
datos2=numpy.zeros((raw,8))
Roll=numpy.zeros(((raw+1),4))
Pitch=numpy.zeros(((raw+1),4))
euler=numpy.zeros((raw,6))


value = input("\nQuiere adquirir los datos S/N \n\n")
if value == 'S' or 's' :
    print("\nCapturando datos\n")
    data.write(b'H')
    for i in range(99):
        rec=data.readline()
        #print(rec)
        rec=rec.decode("utf-8")
        #print(rec)
        rec=rec.split()
        #print(rec)
        datos[i][:]=rec
    print("\nTermina\n")
    #print(datos,"\n")
    offsets=[numpy.mean(datos[:,2]), numpy.mean(datos[:,3]), numpy.mean(datos[:,4])-(32768/2), numpy.mean(datos[:,5]), numpy.mean(datos[:,6]), numpy.mean(datos[:,7])]

    datos1=deepcopy(datos)
    datos2=deepcopy(datos)

    #no calibrados
    for i in range(0,3):
        for j in range(0,raw):
            datos1[j][i+2]=(datos1[j,i+2])*SENSITIVI_ACCEL
            datos1[j][i+5]=(datos1[j,i+5])*SENSITIVI_GYRO
    #calibrados
    for i in range(0,3):
        for j in range(0,raw):
            datos2[j][i+2]=((datos2[j,i+2])-offsets[1])*SENSITIVI_ACCEL
            datos2[j][i+5]=((datos2[j,i+5])-offsets[i+3])*SENSITIVI_GYRO

    #angulos

    for i in range(0,raw):
        Roll[i][0]=1
        Pitch[i][0]=1
        #acelerometrp
        Roll[i+1][1]=(math.atan2(datos2[i,3],datos2[i,4]))*rad2dec
        Pitch[i+1][1]=(math.atan2(-datos2[i,2],math.sqrt((datos2[i,3]*datos2[i,3])+(datos2[i,4]*datos2[i,4]))))*rad2dec
        #giroscopio
        Roll[i+1][2]=Roll[i][3]+((datos2[i,5]*dt)*rad2dec)
        Pitch[i+1][2]=(Pitch[i][3])+((datos2[i,6])*(dt)*(rad2dec))
        #filtro complementario
        Roll[i+1][3]=(A*Roll[i+1][2])+(B*Roll[i+1][1])
        Pitch[i+1][3]=(A*Pitch[i+1][2])+(B*Pitch[i+1][1])
        #vector
        euler[i][0]=Roll[i+1][1]
        euler[i][1]=Pitch[i+1][1]
        euler[i][2]=Roll[i+1][2]
        euler[i][3]=Pitch[i+1][2]
        euler[i][4]=Roll[i+1][3]
        euler[i][5]=Pitch[i+1][3]
    print(euler, "\n")
    #print(round((euler[99,5]),2))

    for i in range(99):
        conn=requests.get(baseURL+'&field1='+str(round((euler[i][0]),2))+'&field2='+str(round((euler[i][1]),2))+'&field3='+str(round((euler[i][2]),2))+'&field4='+str(round((euler[i][3]),2))+'&field5='+str(round((euler[i][4]),2))+'&field6='+str(round((euler[i][5]),2)))
        conn.close()
        #print("\nEnviando\n")
        time.sleep(1)
    print("\nFin del codigo\n")