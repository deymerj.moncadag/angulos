#include <Wire.h>
 
//Direccion I2C de la IMU
#define MPU 0x68
 
//Ratios de conversion
#define A_R 16384.0 // 32768/2
#define G_R 131.0 // 32768/250
 
//Conversion de radianes a grados 180/PI
#define RAD_A_DEG = 57.295779
 
//MPU-6050 da los valores en enteros de 16 bits
//Valores RAW
int16_t AX, AY, AZ, GX, GY, GZ;


byte buffer_tx[7];

String valores;


void setup()
{
Wire.setSDA(PB7);
Wire.setSCL(PB8);
Wire.begin(); // D2(GPIO4)=SDA / D1(GPIO5)=SCL
Wire.beginTransmission(MPU);
Wire.write(0x6B);
Wire.write(0);
Wire.endTransmission(true);
Serial.begin(115200);
}

void loop()
{ 
    if(Serial.available()){
    if (Serial.read()== 'H'){
      int cont=1;
      while(cont<100){
        
   //Leer los valores del Acelerometro de la IMU
   Wire.beginTransmission(MPU);
   Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
   Wire.endTransmission(false);
   Wire.requestFrom(MPU,6,true);   //A partir del 0x3B, se piden 6 registros
   AX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
   AY=Wire.read()<<8|Wire.read();
   AZ=Wire.read()<<8|Wire.read();
 
   //Leer los valores del Giroscopio
   Wire.beginTransmission(MPU);
   Wire.write(0x43);
   Wire.endTransmission(false);
   Wire.requestFrom(MPU,6,true);   //A partir del 0x43, se piden 6 registros
   GX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
   GY=Wire.read()<<8|Wire.read();
   GZ=Wire.read()<<8|Wire.read();
 
    
   //Mostrar los valores por consola
   valores =String(cont) + " "+ String(cont) + " " + String(AX) + " " + String(AY) + " " + String(AZ) + " " +String(GX) + " " + String(GY) + " " + String(GZ);
   Serial.println(valores);
   cont=cont+1;
 
   delay(10);
      }
    }
    }
   
    }
  
